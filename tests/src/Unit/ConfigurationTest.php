<?php

declare(strict_types=1);

namespace Drupal\Tests\field_highlightjs\Unit;

use Drupal\field_highlightjs\FieldHighlightHelper;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use Symfony\Component\Yaml\Yaml;

/**
 * Unit test to check the configuration settings against the remote.
 *
 * @group field_highlightjs
 * @internal
 */
class ConfigurationTest extends UnitTestCase {

  /**
   * Tests if local and remote languages are still the same.
   */
  public function testLocalagainstRemote(): void {
    foreach (['languages', 'themes'] as $type) {
      $method = 'get' . ucwords($type);
      $request = 'Please update configuration with "drush field_highlightjs:create-release".';
      $local = Yaml::parseFile(__DIR__ . "/../../../config/install/field_highlightjs.$type.yml");
      $highlightHelper = new FieldHighlightHelper(new Client());
      $remote = [
        $type => method_exists($highlightHelper, $method) ? $highlightHelper->$method() : [],
      ];
      $this->assertSame($local, $remote, "Local and remote $type differ. $request");

      // Check if the common languages are present in all languages.
      if ($type === 'languages') {
        $diff = array_diff($highlightHelper->getCommonLanguageKeys(), $local[$type]);
        $this->assertEmpty($diff, "There are common languages missing from the local languages. $request");
      }
    }

  }

}
