<?php

namespace Drupal\field_highlightjs\Commands;

use Drupal\Component\Serialization\Yaml;
use Drupal\field_highlightjs\FieldHighlightHelper;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 */
class FieldHighlightjsCommands extends DrushCommands {

  /**
   * The helper service.
   *
   * @var \Drupal\field_highlightjs\FieldHighlightHelper
   */
  protected $helper;

  /**
   * TokenCommands constructor.
   *
   * @param \Drupal\field_highlightjs\FieldHighlightHelper $helper
   *   The helper service.
   */
  public function __construct(FieldHighlightHelper $helper) {
    $this->helper = $helper;
  }

  /**
   * Create a new release.
   *
   * @command field_highlightjs:create-release
   * @aliases fh:cr
   * @usage fh:cr
   *   Create a new release.
   */
  public function createRelease(): void {
    // Write preview template.
    if ($html = $this->helper->getPreviewHtml()) {
      file_put_contents(__DIR__ . '/../../templates/preview.html', $html);
    }

    // Write themes.
    if ($themes = $this->helper->getThemes()) {
      $yaml = Yaml::encode(['themes' => $themes]);
      file_put_contents(__DIR__ . '/../../config/install/field_highlightjs.themes.yml', $yaml);
    }

    // Write languages.
    if ($languages = $this->helper->getLanguages()) {
      $yaml = Yaml::encode(['languages' => $languages]);
      file_put_contents(__DIR__ . '/../../config/install/field_highlightjs.languages.yml', $yaml);
    }

    // Write settings.
    $versions = $this->helper->getVersions();
    $settings = [
      'theme' => 'default',
      'version' => reset($versions),
      'cdn' => 'cloudflare.com',
      'languages' => [],
    ];

    $common_languages = $this->helper->getCommonLanguageKeys();
    foreach ($languages as $language_key => $language_label) {
      $settings['languages'][$language_key] = array_key_exists($language_key, $common_languages) ? $language_key : 0;
    }

    $settings['languages'] = array_filter($settings['languages']);

    $yaml = Yaml::encode($settings);
    file_put_contents(__DIR__ . '/../../config/install/field_highlightjs.settings.yml', $yaml);

    $this->output()->writeln('Release created. Commit changed files.');

  }

}
