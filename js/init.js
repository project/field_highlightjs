(function (Drupal, once) {
  Drupal.behaviors.fieldHighlightjsInit = {
    attach: function (context, settings) {
      once('highlightjs', 'pre code', context).forEach(function (element) {
        hljs.highlightElement(element);
      });
    }
  };
})(Drupal, once);
