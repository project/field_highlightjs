(function ($, Drupal) {
  Drupal.behaviors.fieldHighlightjsSettings = {
    attach: function (context, settings) {
      // Change the css when the theme select is changed.
      $('#field-highlightjs-settings #edit-theme', context).change(function () {
        theme = $(this).val();
        css = $("head link[href*='highlight.js'][href$='.min.css']" ).first();
        css_current_url = css.attr('href').split('styles/').pop();
        css_url = css.attr('href').replace(css_current_url, theme + '.min.css');
        css.attr('href', css_url);
      });
      // Change the language preview when it is changed.
      $('#field-highlightjs-settings #edit-preview-markup div').hide();
      current_language = $('#field-highlightjs-settings #edit-preview-language').val();
      $('.' + current_language).show();
      $('#field-highlightjs-settings #edit-preview-language', context).change(function () {
        $('#field-highlightjs-settings #edit-preview-markup div').hide();
        codeblock = $('.' + $(this).val());
        codeblock.show();
      });
    }
  };
})(jQuery, Drupal);
